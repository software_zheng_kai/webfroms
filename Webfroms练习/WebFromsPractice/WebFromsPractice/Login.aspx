﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="WebFromsPractice.Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div style="width:280px;margin:auto;">
            <asp:Label runat="server" ID="Username" Text="用户名&nbsp："></asp:Label>
            <asp:TextBox runat="server" ID="txtname"></asp:TextBox>
        </div>
        <div style="width:280px;margin:auto">
            <asp:Label runat="server" ID="Password" Text="密&nbsp &nbsp码 ："></asp:Label>
            <asp:TextBox runat="server" ID="txtpwd" TextMode="Password"></asp:TextBox>
        </div>
          <div style="width:280px;margin:auto;padding-left:90px;margin-top:10px;">
              <asp:Button runat="server" ID="Loging" Text="登录" OnClick="Loging_Click" />
              &nbsp &nbsp &nbsp &nbsp &nbsp 
              &nbsp &nbsp &nbsp &nbsp &nbsp
              <asp:Button runat="server" ID="Sign" Text="注册" OnClick="Sign_Click" />
          </div>
    </form>
</body>
</html>
