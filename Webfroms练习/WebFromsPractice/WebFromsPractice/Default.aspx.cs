﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebFromsPractice
{
    public partial class _Default : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                FillData();         
            }
        }
        public void FillData()
        {
            var sql = "select * from Users";
            var dt = Dbhelper.GetData(sql);
            gva.DataSource = dt;
            gva.DataBind();
        }
    }
}