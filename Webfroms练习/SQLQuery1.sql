create database Admin3000
go
use Admin3000
go

create table Users
(
	Id int primary key identity,
	Username nvarchar(80) not null,
	Password nvarchar(80) not null,
	IsActived bit not null default 1,
	IsDeleted bit not null default 0,
	CreatedTime datetime not null default getdate(),
	UpdatedTime datetime not null default getdate(),
	Remarks nvarchar(800) null
)

go

insert into Users (Username,Password) values ('admin','123')
,('user01','123')
go